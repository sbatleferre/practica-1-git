# practica 1 git


## Clonamos del repositorio
Clonamos el repositorio en mi PC con el comando **git clone [url del repositorio]**

![1.png](https://gitlab.com/sbatleferre/practica-1-git/-/raw/main/git_lab/1.PNG)

## add y commit 
Añadimos el proyecto html con git add **"nombre del archivo"** y hacemos commit con el comando **git commit -m "comentario"**

![2.png](https://gitlab.com/sbatleferre/practica-1-git/-/raw/main/git_lab/2.PNG)

## push
Subimos al repositorio remoto mediante el comando **git push origin main**
![3.png](https://gitlab.com/sbatleferre/practica-1-git/-/raw/main/git_lab/3.PNG)

# GitHub
Proceso en GitHub en: (https://github.com/sergibatle00/practica)
